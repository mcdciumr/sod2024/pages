# Webseiten über Pipelines ausliefern

Pipelines können genutzt werden, um Webseiten zu entwickeln und Updates automatisch zu 
veröffentlichen. Weil das ein sehr häufig verwendeter Workflow ist, gibt es mit 
[Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) ein eigenes Konzept dafür.

```yaml
image: ubuntu:22.04

pages:
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
```

- Einrichtung geht am schnellsten über GitLab UI (**Deploy -> Pages**).
- Spezieller _Job_ für Webseiten: `pages`.
- _Artefakte_ sind Ordner oder einzelne Dateien, die als Ergebnis einer Pipeline gespeichert.
  werden. Sie können auf der Seite heruntergeladen werden, die die bisherigen Pipelines auflistet. 
- Im Kontext von Webseiten gibt `artefacts` an, welcher Ordner die fertige Webseite enthält.
- `public`-Ordner ist Standard für die Dateien der Webseite.
  - Darin müssen alle Dateien liegen, die auf die Seite kopiert werden.
  - Ordner muss entweder im Repository existieren oder während der Pipeline erzeugt werden.
- Eigene Domains und Zertifikate sind möglich, um z.B. `mein-projekt.pages.uni-marburg.de` auf 
  `mein-projekt.de` umzuleiten.
